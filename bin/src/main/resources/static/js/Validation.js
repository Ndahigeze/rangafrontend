/**
 * Created by SULAIMAN on 28/01/2019.
 */
//validating login form
$('#l_form').validate({
    rules: {

        username: {
            required: true,
            minlength:3,
            
        },
        password:{
        	required: true,
        	minlength:3,
        	
        }
       
    },
    messages: {
        username: {
            required: "Please enter the username",
            minlength:"The username should at least have 3 characters",
            
        },
        password:{
        	required: "Please enter the password",
        	minlength:"The password should at least have 3 characters",
        	
        }
        

    },

});

