/**
 * Created by SULAIMAN on 3/15/16.
 */
$(document).ready(function(){

    /***************************************/
    /* Form validation */
    /***************************************/
    $( '#forms-institution' ).validate({
        /* @validation rules */
        rules: {
            name: {
                required: true
            },
            password: {
                required: true,
                minlength: 6
            },
            phone:{
                number:1

            },
            username:{
                required:true
            },
            confirmPassword:{
                required:true,
                minlength:6,
                equalTo:"#password"
            }
        },
        messages: {
            username: {
                required: 'Please enter the username'
            },
            password: {
                required: 'Please enter your password',
                minlength: 'At least 6 characters'
            },
            name:{
                required:"Please enter the name"
            },
            phone:{
                number:"Phone should be number"
            },
            confirmPassword:{
                equalTo:"password mismacth"
            }
        },

    });


    $( '#semsesterForm' ).validate({
        /* @validation rules */
        rules: {
            name: {
                required: true
            }
        },
        messages: {
            name: {
                required: 'Please enter the name'
            },

        },

    });









    $( '#workTypeForm' ).validate({
        /* @validation rules */
        rules: {
            name: {
                required: true
            },supervision:{
                required:true
            }
        },

        messages: {
            name: {
                required: 'Please enter the name'
            },
            supervision:{
                required:"Please choose if it needs to be supervised"
            }

        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "supervision") {
                error.appendTo(".supervisionId");
            } else {
                error.insertAfter(element);
            }
        }

    });


    $( '#departmentForm' ).validate({
        /* @validation rules */
        rules: {
            name: {
                required: true
            }
        },
        messages: {
            name: {
                required: 'Please enter the name'
            },

        },

    });

    $( '#courseForm' ).validate({
        /* @validation rules */
        rules: {
            name: {
                required: true
            },
            teacherId:{
                required:true
            },
            departmentId:{
                required:true
            }
        },
        messages: {
            name: {
                required: 'Please enter the name'
            },
            teacherId:{
                required:"Please choose  a teacher"
            },
            departmentId:{
                required:"Please choose a department"
            }

        },

    });






    $( '#multipleChoiceForm' ).validate({
        /* @validation rules */
        rules: {
            answer: {
                required: true
            },
            correctAnswer:{
                required:true
            },
            needNext:{
                required:true
            }
        },
        messages: {
            answer: {
                required: 'Please enter the answer'
            },
            correctAnswer:{
                required:"Please specify if it's the correct answer"
            },
            needNext:{
                required:"Please specify if it's the last one"
            }

        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "needNext") {
                error.appendTo(".needNextId");
            }else if(element.attr("name") == "correctAnswer"){
                error.appendTo(".correctAnswerId");
            } else {
                error.insertAfter(element);
            }
        }

    });




    $( '#questionForm' ).validate({
        /* @validation rules */
        rules: {
            question: {
                required: true,
                minlength:10,
                maxlength:4500

            },
            mark:{
                required:true,
                number:1
            },
            multipleChoice:{
                required:true
            }
        },
        messages: {
            question: {
                required: 'Please enter the the question',
                minlength:"question should atleast have 10 characters",
                maxlength:"question should not have more tha  4500 characters"
            },
            mark:{
                required:"Please choose   mark for this question",
                number:"mark should be number"
            },
            multipleChoice:{
                required:"Please choose specify if it's multiple choice"
            }

        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "multipleChoice") {
                error.appendTo(".choiceId");
            } else {
                error.insertAfter(element);
            }
        }

    });

    $( '#teacherForm' ).validate({
        /* @validation rules */
        rules: {
            firstName: {
                required: true
            },
            phone:{
                number:1
            },
            gender:{
                required:true
            }
        },
        messages: {
            firstName: {
                required: 'Please enter the first Name'
            },
            phone:{
                number:"Phone should be number"
            },
            gender:{
                required:"Please choose gender"
            }

        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "gender") {
                error.appendTo(".genderId");
            } else {
                error.insertAfter(element);
            }
        }

    });
    $( '#studentForm' ).validate({
        /* @validation rules */
        rules: {
            firstName: {
                required: true
            },
            phone:{
                number:1
            },
            gender:{
                required:true
            },
            studentId:{
                required:true
            },
            departmentId:{
                required:true
            }
        },
        messages: {
            firstName: {
                required: 'Please enter the first Name'
            },
            phone:{
                number:"Phone should be number"
            },
            gender:{
                required:"Please choose gender"
            },
            studentId:{
                required:"Please enter the studentId"
            },
            departmentId:{
                required:"Please choose department"
            }

        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "gender") {
                error.appendTo(".genderId");
            } else {
                error.insertAfter(element);
            }
        }

    });

    $( '#courseworkInstructionForm' ).validate({
        /* @validation rules */
        rules: {
            description: {
                required: true,
                maxlength:4500
            }
        },
        messages: {
            description: {
                required: 'Please enter the instruction',
                maxlength:"Instructions shouldn't exceed 4500 characters"
            }

        },

    });
    /***************************************/
    /* end form validation */
    /***************************************/
});