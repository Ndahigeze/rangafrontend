//$(document).on('click','.delete_item',function () {
//    var path=$("#delete_path").val();
//     var row=$("#clicked_row").val();
//     console.log(path);
//     deleteItem(path,row);
//    $('#delete_modal').modal('hide')
//});

$(function() {
  $("body").tooltip({ selector: "[data-toggle=tooltip]" });
});


function deleteItem(path,listEntities) {
    var username=$.parseJSON(sessionStorage.getItem('a_user')).username;
    
    $.ajax({
        headers: {
            'token': token(),
            'doneBy':username
        },
        url:path,
        type : "DELETE", // type of action POST || GET
        //dataType : 'json', // data type
        contentType: 'application/json',
        success : function(result, textStatus, jQxhr) {
            if(result.CODE==200){
            	
                // console.log(result)
                $.alert(result.DESCRIPTION,
                    {
                        type : "success",
                        position : [ 'top-right',
                            [ -0.42, 0 ] ],
                    });
                listEntities();

            }else{
                $.alert(result.DESCRIPTION,
                    {
                        type : "danger",
                        position : [ 'top-right',
                            [ -0.42, 0 ] ],
                    });
            }


        },
        error: function(xhr, resp, text) {
            console.log(xhr, resp, text);
            $("#e_save").removeClass('disabled');
            $.alert('Error Occurred'+xhr.responseText,
                {
                    type : "danger",
                    position : [ 'top-right',
                        [ -0.42, 0 ] ],
                });

        }
    });
}


function sendMail(email,subject,content){
   var email={
       "email":email,
       "subject":subject,
       "content":content,
   }

    $.ajax({
      
        url: ranga_um() + "/mails/send",
        type: "POST", // type of action POST || GET
        data:JSON.stringify(email),
        dataType : 'json', // data type
        contentType: 'application/json',
        success: function (result, textStatus, jQxhr) {
            console.log(result);
        },
        error: function (xhr, resp, text) {
            console.log(xhr, resp, text);

        }
    });

}

// Notification handler
//  Saving Notification
   function saveNotification(title,user,notification){
       var noti={
           "title":title,
           "user":user,
           "notification":notification,
       }
       $.ajax({
           headers:{
               'ranga_token': token(),
           },
           url: ranga_um() +"/notifications/save",
           type:"POST",
           data:JSON.stringify(noti),
           dataType:'json',
           contentType:'application/json',
           success:function (result,textStatus,jQxhr){
               console.log(noti);
              console.log(result);
           },
           error:function(xhr,resp,text){
               console.log();
           }
       })
   }

   var user="";
    function ViewAllNotifications(userUuid){
        user=userUuid;
      $.ajax({
          headers: {
              'ranga_token': token(),
          },
          url:ranga_um()+"/notifications/user/"+userUuid,
          type:"GET",
          method:"GET",
          success:function(result){
              console.log(result);
             
              var number = 0;
               $("#notificationWrapper").empty()
              $.each(result.OBJECT,function(i,item){
                  number +=1;
                  $("#notificationWrapper").append('<a class="dropdown-item notificationItem"  id="' + item.uuid + '" href="#">' + item.title + ' &nbsp;<span class="text-info"> Click to read More<span></a>');
              })
              $("#notificationNumber").text(number);
          },
          error:function(xhr,resp,text){
              console.log(resp);
          }
      })
    }

    $(document).on("click",".notificationItem",function(){
         var uuid=$(this).attr('id');
             $.ajax({
                 headers: {
                     'ranga_token': token(),
                 },
                 url: ranga_um() + "/notifications/" + uuid,
                 type: "GET",
                //  method: "GET",
                 success: function (result) {
                    
                     console.log("Result Test",result);
                     Swal.fire({
                         title: result.OBJECT.title,
                         text: result.OBJECT.notification,
                        //  imageUrl: 'https://unsplash.it/400/200',
                        //  imageWidth: 400,
                        //  imageHeight: 200,
                        //  imageAlt: 'Custom image',
                         animation: false
                     })
                      ViewAllNotifications(user);
                 },
                 error: function (shr, resp, text) {
                     console.log(shr);
                 }

             })
    })


    // sending phone SMS
    function sendPhoneSms(smsObject){
      $.ajax({
          type:"POST",
          url: ranga_be()+'/notifications/sms',
          contentType:"application/json",
          data:JSON.stringify(smsObject),
          success:function(result){
             console.log(result);
          },
          error:function(){
          }
      })
    }
    

// End of Notification Handler

function phonenumber(phone) {
    var phoneno = /^\+[0-9]?()[0-9](\s|\S)(\d[0-9]{9})$/;
    if (phone.match(phoneno)) {
        return true;
    } else {
        return false;
    }
}


function numberFormat(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function ranga_be() {
    return "http://localhost:8081";
}

function token() {
    return "RANGAa432f301-2322-409f-9e73-b5933fa177ecr";
}
function ranga_um() {
    return "http://localhost:8083";
}
   
