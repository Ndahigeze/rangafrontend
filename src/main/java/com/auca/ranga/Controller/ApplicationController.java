package com.auca.ranga.Controller;

import javax.servlet.http.HttpSession;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.auca.ranga.Controller.RestController.SystemUser;

@Controller
public class ApplicationController {

	
	/*
	 * Method to return login page 
	 * */
	@RequestMapping("/")
	public String login(){
		
		return "landing/HomePage";
	}

	
	@RequestMapping("/{page}")
	public String getPages(@PathVariable("page")String page, HttpSession session, Model model){
		if(session.getAttribute("a_user")!=null) {
			SystemUser user = (SystemUser) session.getAttribute("a_user");
			if (page.equalsIgnoreCase("properties")) {
                 if (user.getRole().equalsIgnoreCase("Agent") || user.getRole().equalsIgnoreCase("Client")) {
					return "/Property/Properties";
				} else {
					return "_noAccess";
				}		
			} else  if(page.equalsIgnoreCase("newproperty")){
				if(user.getRole().equalsIgnoreCase("Agent")|| user.getRole().equalsIgnoreCase("Client")){
					 return "/Property/NewProperty";
				  }else{
                    return "_noAccess";
				}
				
			}else if(page.equalsIgnoreCase("home")){
               	return "Landing/HomePage";
			}else if(page.equalsIgnoreCase("dashboard")){
				if(user.getRole().equalsIgnoreCase("Admin")){
					return "Adminpages/AdminDashboard";
				}else{
                  	return "Dashboard";
				}
			}else if(page.contains("propertyDetails")){
                    
					if (user.getRole().equalsIgnoreCase("Agent") || user.getRole().equalsIgnoreCase("Client")) {
					   String rguuid[] = page.split("_");
					String uuid = rguuid[1];
					model.addAttribute("rpuuid", uuid);
					return "Property/PropertyDetails";
					} else {
						return "_noAccess";
					}  
			}else if(page.contains("frontDetails")){
				String rguuid[] = page.split("_");
				String uuid = rguuid[1];
				model.addAttribute("rpuuid", uuid);
				return "Property/ClientDetails";
			}else if(page.equalsIgnoreCase("requests")){
				
				if (user.getRole().equalsIgnoreCase("Agent") || user.getRole().equalsIgnoreCase("Client")) {
					return "Request/Requests";
				} else {
					return "_noAccess";
				}
			}else if(page.contains("requestDetails")){
				
				if (user.getRole().equalsIgnoreCase("Agent") || user.getRole().equalsIgnoreCase("Client")) {
					String rguuid[] = page.split("_");
					String uuid = rguuid[1];
					model.addAttribute("ruuid", uuid);
					return "Request/Details";
				} else {
					return "_noAccess";
				}
			}else if(page.equalsIgnoreCase("renewrequests")){
				if (user.getRole().equalsIgnoreCase("Agent") || user.getRole().equalsIgnoreCase("Client")) {
					return "Request/RenewRequest";
				} else {
					return "_noAccess";
				}
				
			}else if(page.contains("renewRequestDetails")){ 
				 if(user.getRole().equalsIgnoreCase("Agent")|| user.getRole().equalsIgnoreCase("Client")){
                      String rguuid[] = page.split("_");
				      String uuid = rguuid[1];
			     	  model.addAttribute("ruuid", uuid);
				    return "Request/RenewDetails";
			      }else{
					  return "_noAccess";
				  }
			}else if(page.equalsIgnoreCase("profile")){
                   return "User_Management/ProfilePage";
			}else if(page.equalsIgnoreCase("receivedrequests")){
			   if(user.getRole().equalsIgnoreCase("Agent")|| user.getRole().equalsIgnoreCase("Client")){
                   return "Request/ReceivedRequest";
			   }else{
                   return "_noAccess";
			   }
			}else if(page.equalsIgnoreCase("payments")){
              if(user.getRole().equalsIgnoreCase("Admin")){
               return "Adminpages/Payments";
			  }else{
				  return "_noAccess";
			  }
			}else if(page.equalsIgnoreCase("users")){
               if (user.getRole().equalsIgnoreCase("Admin")) {
					return "User_Management/Users";
				}else{
					return "_noAccess";
				}
			}else if(page.contains("activate")){
                 String info[] = page.split("_");
				String uname = info[1];
				String token = info[2];
				model.addAttribute("username", uname);
				model.addAttribute("token", token);
				return "/Landing/Login";
			}else if(page.equalsIgnoreCase("reports")){
                 return "Reports";
			}else if(page.equalsIgnoreCase("subscription")){
                   if(user.getRole().equalsIgnoreCase("Admin")){
                       return "Adminpages/Subscription";
				   }else{
					   return "_noAccess";
				   }
			}else{
				return "_noAccess";
			}
           
		}else{
			if(page.equalsIgnoreCase("home")){
                return "Landing/HomePage";
			}else if(page.equalsIgnoreCase("login")){
              return "/Landing/Login";
			}else if(page.equalsIgnoreCase("creationAccount")){
              return "/Landing/Registration.html";
			}else if(page.contains("frontDetails")){
				String rguuid[] = page.split("_");
				String uuid = rguuid[1];
				model.addAttribute("rpuuid", uuid);
                return "/Property/ClientDetails";
			}else if(page.contains("activate")){
			      String info[] = page.split("_");
				  String uname= info[1];
				  String token=info[2];
					model.addAttribute("username", uname);
					model.addAttribute("token", token);
				return "/Landing/Login";
			}else{
				return "/Landing/Login";
			}
		}

	}
	

}
