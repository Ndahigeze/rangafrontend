package com.auca.ranga.Controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;




@org.springframework.web.bind.annotation.RestController
public class RestController {

	
	/*
	 * Add user to session
	 * */
	@RequestMapping(value="/add_user/session", method=RequestMethod.POST)
	public Object add_user_to_session(HttpSession session, @RequestParam Map<String,String> map){
		try{
			SystemUser user=new SystemUser();
			user.setAddress(map.get("address"));
			user.setEmailAddress(map.get("emailAddress"));
			user.setDoneAt(new SimpleDateFormat("dd-MM-yyyy").parse(map.get("doneAt")));
			user.setDoneBy(map.get("doneBy"));
			user.setNames(map.get("names"));
			if(map.get("enabled").equals(true)){
				user.setEnabled(true);
			}else{
				user.setEnabled(false);
			}
			user.setPhone(map.get("phone"));
			user.setLastUpdatedAt(new SimpleDateFormat("dd-MM-yyyy").parse(map.get("lastUpdatedAt")));
			user.setId(Long.parseLong(map.get("id")));
			user.setRole(map.get("role"));
			user.setUuid(map.get("uuid"));
			user.setProfilePicture(map.get("profilePicture"));
		session.setAttribute("a_user", user);

		return "OK";
		}catch (Exception e) {
			e.printStackTrace();
			return ""+e.getMessage();
		}
		
	}
	
	/*
	 * 
	 * Log out
	 * */
	
	@RequestMapping(value="/logout", method=RequestMethod.GET)
	public Object logout(HttpSession session, @RequestParam Map<String,String>param){
		try{		
		session.removeAttribute("a_user"); 
		session.removeAttribute("a_reg"); 		
		return "OK";
		}catch (Exception e) {
			return ""+e.getMessage();
		}
		
	}
	
    

	
	public static class SystemUser{
		private long id;
		private String uuid;
		private String names;
		private String emailAddress;
		private String role;
		private boolean userLocked;
		private String reasonForLock;

		private Date doneAt;
		private String doneBy;
		private Date lastUpdatedAt;
		private String lastUpdatedBy;
		private boolean deletedStatus;
		private String phone;
		private String address;
		private String profilePicture;
		private boolean enabled;

		public String getNames() {
			return names;
		}

		public String getUuid() {
			return uuid;
		}

		public void setUuid(String uuid) {
			this.uuid = uuid;
		}

		public void setNames(String names) {
			this.names = names;
		}

		public String getRole() {
			return role;
		}

		public void setRole(String role) {
			this.role = role;
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		/**
		 * @return String return the emailAddress
		 */
		public String getEmailAddress() {
			return emailAddress;
		}

		/**
		 * @param emailAddress the emailAddress to set
		 */
		public void setEmailAddress(String emailAddress) {
			this.emailAddress = emailAddress;
		}


		/**
		 * @return boolean return the userLocked
		 */
		public boolean isUserLocked() {
			return userLocked;
		}

		/**
		 * @param userLocked the userLocked to set
		 */
		public void setUserLocked(boolean userLocked) {
			this.userLocked = userLocked;
		}

		/**
		 * @return String return the reasonForLock
		 */
		public String getReasonForLock() {
			return reasonForLock;
		}

		/**
		 * @param reasonForLock the reasonForLock to set
		 */
		public void setReasonForLock(String reasonForLock) {
			this.reasonForLock = reasonForLock;
		}

		/**
		 * @return Date return the doneAt
		 */
		public Date getDoneAt() {
			return doneAt;
		}

		/**
		 * @param doneAt the doneAt to set
		 */
		public void setDoneAt(Date doneAt) {
			this.doneAt = doneAt;
		}

		/**
		 * @return String return the doneBy
		 */
		public String getDoneBy() {
			return doneBy;
		}

		/**
		 * @param doneBy the doneBy to set
		 */
		public void setDoneBy(String doneBy) {
			this.doneBy = doneBy;
		}

		/**
		 * @return Date return the lastUpdatedAt
		 */
		public Date getLastUpdatedAt() {
			return lastUpdatedAt;
		}

		/**
		 * @param lastUpdatedAt the lastUpdatedAt to set
		 */
		public void setLastUpdatedAt(Date lastUpdatedAt) {
			this.lastUpdatedAt = lastUpdatedAt;
		}

		/**
		 * @return String return the lastUpdatedBy
		 */
		public String getLastUpdatedBy() {
			return lastUpdatedBy;
		}

		/**
		 * @param lastUpdatedBy the lastUpdatedBy to set
		 */
		public void setLastUpdatedBy(String lastUpdatedBy) {
			this.lastUpdatedBy = lastUpdatedBy;
		}

		/**
		 * @return boolean return the deletedStatus
		 */
		public boolean isDeletedStatus() {
			return deletedStatus;
		}

		/**
		 * @param deletedStatus the deletedStatus to set
		 */
		public void setDeletedStatus(boolean deletedStatus) {
			this.deletedStatus = deletedStatus;
		}

		/**
		 * @return String return the phone
		 */
		public String getPhone() {
			return phone;
		}

		/**
		 * @param phone the phone to set
		 */
		public void setPhone(String phone) {
			this.phone = phone;
		}

		/**
		 * @return String return the address
		 */
		public String getAddress() {
			return address;
		}

		/**
		 * @param address the address to set
		 */
		public void setAddress(String address) {
			this.address = address;
		}

		/**
		 * @return String return the profilePicture
		 */
		public String getProfilePicture() {
			return profilePicture;
		}

		/**
		 * @param profilePicture the profilePicture to set
		 */
		public void setProfilePicture(String profilePicture) {
			this.profilePicture = profilePicture;
		}

		/**
		 * @return boolean return the enabled
		 */
		public boolean isEnabled() {
			return enabled;
		}

		/**
		 * @param enabled the enabled to set
		 */
		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}
			
	}
	
	

}
